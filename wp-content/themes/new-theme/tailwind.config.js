const colors = require('tailwindcss/colors')

module.exports = {
    purge: {
        enabled: true,
        content: [
            './*.php',
            './template-parts/*.php',
        ],
    },
    theme: {
        colors: {
            sky: colors.sky,
            cyan: colors.cyan,
            gray: colors.gray,
            white: colors.white,
        },
        extend: {
            spacing: {
                '128': '32rem',
                '144': '36rem',
            },
            borderRadius: {
                '4xl': '2rem',
            }
        },
    },
    variants: {
        extend: {
            borderColor: ['focus-visible'],
            opacity: ['disabled'],
        },
    },
    plugins: [],
};
