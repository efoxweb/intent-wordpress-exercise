<?php

define('EFX_THEME_VERSION', '0.0.1');

function efx_theme_scripts() {
    $themeAssetsFileName = wp_get_environment_type() === 'development' ? 'main' : 'main.min';
    wp_enqueue_style( 'efx-theme-style', get_template_directory_uri() . '/dist/css/' . $themeAssetsFileName . '.css', array(), EFX_THEME_VERSION);
    wp_enqueue_script( 'efx-theme-script', get_template_directory_uri() . '/dist/js/' . $themeAssetsFileName . '.js', array(), EFX_THEME_VERSION);
}

add_action( 'wp_enqueue_scripts', 'efx_theme_scripts' );

function efx_theme_setup() {

    add_theme_support( 'post-thumbnails' );

    register_nav_menus( array(
        'primary' => esc_html__( 'Main Menu', 'wp-theme' ),
        'secondary' => esc_html__( 'Footer Menu', 'wp-theme' ),
    ) );

}

add_action( 'after_setup_theme', 'efx_theme_setup' );