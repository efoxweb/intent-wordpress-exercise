const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");

const isProduction = process.env.NODE_ENV === 'production';

const stylesHandler = MiniCssExtractPlugin.loader;

const config = {
    entry: [
        './src/js/index.js',
        './src/css/theme.css'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
            },
            {
                test: /\.css$/i,
                use: [stylesHandler,'css-loader', 'postcss-loader'],
            },
        ],
    },
    plugins: []
};

module.exports = () => {
    if (isProduction) {
        config.mode = 'production';
        config.plugins.push(
        new MiniCssExtractPlugin({
            filename: 'css/[name].min.css'
        }),);
        config.output.filename = 'js/main.min.js';
        config.optimization = {
            minimizer: [
                new TerserPlugin(),
                new CssMinimizerPlugin({
                    minimizerOptions: {
                        preset: [
                            "default",
                            {
                                discardComments: { removeAll: true },
                            },
                        ],
                    },
                }),
            ],
        }
    } else {
        config.mode = 'development';
        config.watch = true;
        config.devtool = false;
        config.plugins.push(
            new MiniCssExtractPlugin({
                filename: 'css/[name].css'
            }),);
        config.output.filename = 'js/main.js';
    }
    return config;
};
