<figure class="bg-white mb-8 m-auto rounded-xl p-8 w-80">
    <a href="<?= get_permalink(); ?>">
        <img class="w-32 h-32 rounded-full mx-auto" src="<?= get_the_post_thumbnail_url(); ?>" alt="" width="384" height="512">
        <div class="pt-6 text-center space-y-4">
            <blockquote>
                <p class="text-lg font-semibold">
                    “<?= get_the_excerpt(); ?>"
                </p>
            </blockquote>
            <figcaption class="font-medium">
                <div class="text-cyan-600">
                    <?= get_the_title(); ?>
                </div>
                <div class="text-gray-500">
                    Web Developer, UK
                </div>
            </figcaption>
        </div>
    </a>
</figure>