# 🚀 Welcome to your new awesome project!

This project has been created using **webpack-cli**, you can now run

```
yarn
```

Production

```
yarn run build
```

Development

```
yarn build:dev
```

to bundle your application
