<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gutenberg-starter-theme
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= wp_title(); ?></title>
    <?php wp_head(); ?>
</head>
<body>
<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'gutenberg-starter-theme' ); ?></a>
<div class="site-body">
    <header class="site-header">
        <div class="header-inner">
            <div class="header-branding">
                <div class="header-logo">
                    <a href="<?= home_url(); ?>">LOGO</a>
                </div>
            </div>
            <nav id="site-navigation" data-nav>
                <?php wp_nav_menu( array(
                    'container'       => 'ul',
                    'depth'           => 0,
                    'theme_location'  => 'primary',
                    'menu_class'      => 'menu',
                ) ); ?>
            </nav>
        </div>
    </header>