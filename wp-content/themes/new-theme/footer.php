<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gutenberg-starter-theme
 */

?>
</div>
<footer class="site-footer">
    <div class="container">
        <nav id="footer-navigation">
            <?php wp_nav_menu( array(
                'container'       => 'ul',
                'depth'           => 1,
                'theme_location'  => 'secondary',
                'menu_class'      => 'menu',
            ) ); ?>
        </nav>
    <div class="site-info">
        <?php get_option('efx-footer-content'); ?>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
