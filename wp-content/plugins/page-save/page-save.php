<?php
/**
 * Plugin Name: E-FOX Page Save Plugin
 * Plugin URI: https://www.rickmiddleton.co.uk
 * Description: Test
 * Version: 0.0.1
 * Author: Rick Middleton
 * Author URI: https://www.rickmiddleton.co.uk
 * Text Domain: theme
 *
 */

defined( 'ABSPATH' ) || exit;

if ( is_readable( __DIR__ . '/vendor/autoload.php' ) ) {
    require __DIR__ . '/vendor/autoload.php';
}

function init_save_pages_plugin() {

    require_once plugin_dir_path( __FILE__ ) . 'includes/class-page-save.php';

    $savePages = new SAVE_PAGES();

    register_activation_hook( __FILE__, array($savePages, 'activate_plugin'));

    register_deactivation_hook( __FILE__, array($savePages, 'deactivate_plugin'));

}

init_save_pages_plugin();
