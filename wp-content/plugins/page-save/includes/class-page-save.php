<?php class SAVE_PAGES {

    const FILEPATH = 'page-save/pages.json';
    const LIMIT = 20;

    private $jsonFilePath;

    function __construct() {
        $wpUploadDir = wp_upload_dir();
        $this->jsonFilePath = $wpUploadDir['basedir'] . '/' . self::FILEPATH;
    }

    private function get_page_list() {

        $pageList = get_posts(
            [
                'post_type' => 'page',
                'posts_per_page' => self::LIMIT,
                'orderby' => 'title',
                'order' => 'ASC'
            ]
        );

        if (!$pageList) return false;

        return $pageList;

    }

    private function save_page_list_json_file($jsonContent) {
        $dirname = dirname($this->jsonFilePath);
        if (!is_dir($dirname)) wp_mkdir_p($dirname);
        $jsonFile = fopen($this->jsonFilePath, "w") or die("Unable to write to file!");
        fwrite($jsonFile, $jsonContent);
        fclose($jsonFile);
    }

    private function remove_page_list_json_file() {
        if(is_file($this->jsonFilePath)) unlink($this->jsonFilePath);
        $dirname = dirname($this->jsonFilePath);
        if (is_dir($dirname)) rmdir($dirname);
    }

    private function convert_pagelist_to_json($pageList) {

        if (!$pageList) return false;

        $jsonPages = [];

        foreach($pageList as $page) {
            $jsonPages[] = (Object) [
                'ID' => $page->ID,
                'post_title' => $page->post_title,
            ];
        }

        return json_encode($jsonPages);

    }

    public function activate_plugin() {
        $pageListArr = $this->get_page_list();
        $jsonContent = $this->convert_pagelist_to_json($pageListArr);
        if ($jsonContent) $this->save_page_list_json_file($jsonContent);
    }


    public function deactivate_plugin() {
        $this->remove_page_list_json_file();
    }

}